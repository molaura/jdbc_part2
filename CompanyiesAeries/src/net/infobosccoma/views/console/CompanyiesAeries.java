package net.infobosccoma.views.console;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.infobosccoma.model.business.entities.Avio;
import net.infobosccoma.model.business.entities.Companyia;
import net.infobosccoma.model.persistence.daos.contracts.AvioDAO;
import net.infobosccoma.model.persistence.daos.contracts.CompanyiaDAO;
import net.infobosccoma.model.persistence.factories.DAOFactory;
import net.infobosccoma.model.persistence.utilities.DataSourceEnum;

public class CompanyiesAeries {

    public static void main(String[] args) {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DataSourceEnum.JDBC);
        CompanyiaDAO companyiaDAO = daoFactory.getCompanyiaDAO();
        AvioDAO avioDAO = daoFactory.getAvioDAO();


        /* ---------  EXEMPLE CREACIÓ D'OBJECTES I INSERCIÓ A LA BD ----------*/
//        List<Companyia> companyiaList = new ArrayList();
//
//        companyiaList.add(new Companyia("companyiazero"));
//        companyiaList.add(new Companyia("companyiaone"));
//        companyiaList.add(new Companyia("companyiadue"));
//        companyiaList.add(new Companyia("companyiatres"));
//        for (Companyia companyia : companyiaList) {
//           companyia.setId(companyiaDAO.create(companyia));
//        }
//        /* ---------  EXEMPLE OBTENCIÓ D'OBJECTES I LLISTAT Companyia getAll ----------*/
        Map<Integer, Companyia> map = companyiaDAO.getAll();
        llistarCompanyia(map);


        /* ---------  EXEMPLE ACTUALITZACIÓ D'OBJECTES  Companyia Update ----------*/
        System.out.println("MODIFICAR LA COMPANYIA AMB ID = 4");
        map.get(2).setNom("avioupdated");
        if (companyiaDAO.update(map.get(2))) {
            System.out.println("DADES ACTUALITZADES CORRECTAMENT");
        } else {
            System.out.println("NO S'HAN ACTUALITZAT LES DADES");
        }

        /* ---------  EXEMPLE OBJTENCIÓ D'OBJECT PER ID ----------*/
        System.out.println("MOSTRAR LA COMPANYIA AMB ID = 4:");
        System.out.println(companyiaDAO.getById(2));

        /* ---------  EXEMPLE D'ESBORRAT D'OBJECTES ----------*/
        System.out.println("ESBORRAR LA COMPANYIA AMB ID = 7");
        if (companyiaDAO.delete(1)) {
            System.out.println("COMPANYIA ESBORRADA CORRECTAMENT");
        } else {
            System.out.println("NO S'HAN ESBORRAT LA COMPANYIA");
        }
        System.out.println(companyiaDAO.getById(2));
        llistarCompanyia(companyiaDAO.getAll());
 
        /*****************************  AVIONS ********************************************/

//        /* ---------  EXEMPLE OBTENCIÓ D'OBJECTES I LLISTAT Companyia getAll ----------*/
//        Map<Integer, Avio> mapavio = avioDAO.getAll();
//        llistarAvions(mapavio);
//
//
//        /* ---------  EXEMPLE ACTUALITZACIÓ D'OBJECTES  Companyia Update ----------*/
//        System.out.println("MODIFICAR LA AVIO AMB ID = 2");
//        mapavio.get(2).setNom("avioupdated");
//        if (avioDAO.update(mapavio.get(2))) {
//            System.out.println("DADES ACTUALITZADES CORRECTAMENT");
//        } else {
//            System.out.println("NO S'HAN ACTUALITZAT LES DADES");
//        }
//
//        /* ---------  EXEMPLE OBJTENCIÓ D'OBJECT PER ID ----------*/
//        System.out.println("MOSTRAR LA COMPANYIA AMB ID = 4:");
//        System.out.println(avioDAO.getById(4));
//
//        /* ---------  EXEMPLE D'ESBORRAT D'OBJECTES ----------*/
//        System.out.println("ESBORRAR LA COMPANYIA AMB ID = 7");
//        if (avioDAO.delete(4)) {
//            System.out.println("COMPANYIA ESBORRADA CORRECTAMENT");
//        } else {
//            System.out.println("NO S'HAN ESBORRAT LA COMPANYIA");
//        }
//
//        System.out.println(avioDAO.getById(4));
//        llistarAvions(avioDAO.getAll());
//

    
    }

    /**
     * llista el contingut d'un map
     *
     * @param map
     */
    static void llistarCompanyia(Map<Integer, Companyia> map) {
        map.keySet().stream().forEach((id) -> {
            System.out.println(map.get(id));
        });
    }
    
    //************* AVIONS **************//
    /**
     * llista el contingut d'un map
     *
     * @param map
     */
    static void llistarAvions(Map<Integer, Avio> map) {
        map.keySet().stream().forEach((id) -> {
            System.out.println(map.get(id));
        });
    }

}
