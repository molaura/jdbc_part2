/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.factories;

import net.infobosccoma.model.persistence.daos.contracts.AvioDAO;
import net.infobosccoma.model.persistence.daos.impl.jdbc.JDBCAvioDAO;
import net.infobosccoma.model.persistence.daos.contracts.CompanyiaDAO;
import net.infobosccoma.model.persistence.daos.impl.jdbc.JDBCCompanyiaDAO;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
class JDBCDAOFactory  extends DAOFactory{
    
    
    @Override
    public CompanyiaDAO getCompanyiaDAO() {
        return new JDBCCompanyiaDAO();
    }

    @Override
    public AvioDAO getAvioDAO() {
        return new JDBCAvioDAO();
    }

}
