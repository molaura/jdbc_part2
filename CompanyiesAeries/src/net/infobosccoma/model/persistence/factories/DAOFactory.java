/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.factories;

import net.infobosccoma.model.persistence.daos.contracts.CompanyiaDAO;
import net.infobosccoma.model.persistence.daos.contracts.AvioDAO;
import net.infobosccoma.model.persistence.utilities.DataSourceEnum;

/**
 *
 * @author usuari
 */
public abstract class DAOFactory {

    public abstract CompanyiaDAO getCompanyiaDAO();
    public abstract AvioDAO getAvioDAO();
/**
 * 
 * @param database (la base de dades escollida i passada per parametre)
 * @return el Factory del tipus de base de dades escollit
 */
public static DAOFactory getDAOFactory(DataSourceEnum database) {
        switch (database) {
        case JDBC:
            return new JDBCDAOFactory();
        case HIBERNATE:
             return new HibernateDAOFactory();
        case JSON:
             return new JSONDAOFactory();
        default:
            return null;
        }
    }
}
