/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.daos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public class JDBCConnection {

    private final String ORACLE_CONFIG_FILE = "config/oracle.properties";

    private Connection connection;
    private static JDBCConnection db;


    private JDBCConnection() {
        try (FileInputStream in = new FileInputStream(ORACLE_CONFIG_FILE)) {
            Properties properties = new Properties();
            properties.load(in);
            
            String url = properties.getProperty("dburl");
            String driver = properties.getProperty("dbdriver");
            String userName = properties.getProperty("dbuser");
            String password = properties.getProperty("dbpassword");
            try {
                Class.forName(driver).newInstance();
                this.connection = (Connection) DriverManager.getConnection(url, userName, password);
            } catch (Exception sqle) {
                sqle.printStackTrace();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

/**
 * 
 * @return La connexió a la base de dades sql
 */
        public static Connection getConnection() {
        if (db == null) {
            db = new JDBCConnection();
        }
        return db.connection;
    }

}
