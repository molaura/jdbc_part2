/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.daos.contracts;

import java.util.Map;
import net.infobosccoma.model.business.entities.Avio;
import net.infobosccoma.model.business.entities.Companyia;

/**
 *
 * @author usuari
 */
public interface AvioDAO extends DAO<Integer,Avio>{
    
    public Map<Integer,Avio> getAllByCompanyia(Companyia companyia);
}
