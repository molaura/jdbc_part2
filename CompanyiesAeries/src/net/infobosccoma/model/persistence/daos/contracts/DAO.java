/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.daos.contracts;

import java.util.Map;

/**
 *
 * @author usuari
 * @param <K>
 * @param <O>
 */
public interface DAO<K, O> {
    
    /*COMPANYIA I AVIO*/
    public K create(O object);
   public O getById(K key);
    public boolean update(O object);
   public boolean delete(K key);
    public Map<K,O> getAll();
    
//    /*AVIO*/
//    public Map<K,O> mapQueryAvions();
//  public Map<K,O> getAllByCompanyia();
//    
//    /*COMPANYIA*/
//    public Map<K,O> mapQueryCompanyia();
//    public Map<K,O> getAvionsByCompanyia();
//    
}
