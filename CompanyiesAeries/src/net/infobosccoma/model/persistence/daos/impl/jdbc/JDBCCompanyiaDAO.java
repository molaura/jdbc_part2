/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.daos.impl.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.infobosccoma.model.business.entities.Companyia;
import net.infobosccoma.model.business.entities.Avio;
import net.infobosccoma.model.persistence.daos.JDBCConnection;
import net.infobosccoma.model.persistence.daos.contracts.AvioDAO;
import net.infobosccoma.model.persistence.daos.contracts.CompanyiaDAO;
import net.infobosccoma.model.persistence.factories.DAOFactory;
import net.infobosccoma.model.persistence.utilities.DataSourceEnum;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public class JDBCCompanyiaDAO implements CompanyiaDAO {

    private final String SELECT_ALL_STR = "SELECT * FROM COMPANYIA ORDER BY 1";
    private final String SELECT_BY_ID_STR = "SELECT * FROM COMPANYIA WHERE id = ?";
    private final String UPDATE_STR = "UPDATE COMPANYIA SET nom = ? WHERE id = ?";
    private final String INSERT_STR = "INSERT INTO COMPANYIA (nom) VALUES(?)";
    private final String DELETE_STR = "DELETE FROM COMPANYIA WHERE id = ?";

    /**
     *
     * @param object
     * @return l'dentificador que s'ha creat
     */
    @Override
    public Integer create(Companyia object) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(INSERT_STR)) {
            query.setInt(1, object.getId());
            query.setString(2, object.getNom());

            rowsUpdated = query.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (int) rowsUpdated;
    }

    /**
     *
     * @param key
     * @return l'objecte Companyia que correspon a la clau primària
     */
    @Override
    public Companyia getById(Integer key) {
        Map<Integer, Companyia> map = null;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(SELECT_BY_ID_STR)) {
            query.setInt(1, key);
            map = mapQueryCompanyia(query);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map == null ? null : map.get(key);
    }

    /**
     *
     * @param object
     * @return true si ha pogut actualitzar, false si no ha pogut
     */
    @Override
    public boolean update(Companyia object) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(UPDATE_STR)) {
            query.setString(1, object.getNom());
            query.setInt(2, object.getId());

            rowsUpdated = query.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowsUpdated > 0;
    }

    /**
     *
     * @param key
     * @return true si ha pogut eliminar, false si no
     */
    @Override
    public boolean delete(Integer key) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(DELETE_STR)) {
            query.setInt(1, key);
            rowsUpdated = query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowsUpdated > 0;
    }

    /**
     *
     * @return els objectes guardats en forma de map ordenat per l'id Companyia
     */
    @Override
    public Map<Integer, Companyia> getAll() {
        Map<Integer, Companyia> map = null;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(SELECT_ALL_STR)) {
            map = (Map<Integer, Companyia>) mapQueryCompanyia(query);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     *
     * @param query
     * @return les dades obtingudes de la consulta en forma de map
     */
    private Map<Integer, Companyia> mapQueryCompanyia(PreparedStatement query) {
        Map<Integer, Companyia> map = null;
        Companyia companyia;

        try (ResultSet results = query.executeQuery()) {
            map = new TreeMap<>();
            while (results.next()) {
                companyia = new Companyia(results.getInt("id"),
                        results.getString("nom"));

                // afegir la companyia
                map.put(results.getInt("id"), companyia);
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCompanyiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     *
     * @param companyia
     * @return els avions associats a la companyia indicada
     */
    private Map<Integer, Avio> getAvionsByCompanyia(Companyia companyia) {
        DAOFactory daoFactory = DAOFactory.getDAOFactory(DataSourceEnum.JDBC);
        AvioDAO avioDAO = daoFactory.getAvioDAO();
        return avioDAO.getAllByCompanyia(companyia);
    }

}
