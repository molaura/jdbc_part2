/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.daos.impl.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.infobosccoma.model.business.entities.Avio;
import net.infobosccoma.model.business.entities.Companyia;
import net.infobosccoma.model.persistence.daos.JDBCConnection;
import net.infobosccoma.model.persistence.daos.contracts.AvioDAO;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public class JDBCAvioDAO implements AvioDAO {

    private final String SELECT_BY_AVIOID_STR = "SELECT * FROM Avio WHERE idAvio= ?";
    private final String SELECT_BY_ID_STR = "SELECT * FROM Avio WHERE id = ?";
    private final String SELECT_ALL_STR = "SELECT * FROM Avio";
    private final String UPDATE_STR = "UPDATE Avio SET nom = ?, nseients = ?, model = ?, datafabricacio = ?, idcompanyia = ? WHERE id = ?";
    private final String INSERT_STR = "INSERT INTO Avio(nom, nseients, model, datafabricacio, idcompanyia) " + "VALUES(?,?,?,?,?)";
    private final String DELETE_STR = "DELETE FROM Avio WHERE id = ?";

    /**
     *
     * @param object
     * @return l'dentificador que s'ha creat
     */
    @Override
    public Integer create(Avio object) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(INSERT_STR)) {
            query.setString(1, object.getNom());
            query.setInt(2, object.getnSeients());
            query.setString(3, object.getModelAvio());
            query.setDate(4, java.sql.Date.valueOf(object.getDataFabricacio()));
            query.setInt(5, object.getCompanyia().getId());
            rowsUpdated = query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (int) rowsUpdated;
    }

    /**
     *
     * @param id
     * @return l'objecte Avió que correspon a la clau primària
     */
    @Override
    public Avio getById(Integer id) {
        Map<Integer, Avio> map = null;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(SELECT_BY_ID_STR)) {
            query.setInt(1, id);
            map = mapQueryAvions(query);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map == null ? null : map.get(id);
    }

    /**
     *
     * @param avio
     * @return true si ha pogut actualitzar, false si no ha pogut.
     */
    @Override
    public boolean update(Avio avio) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(UPDATE_STR)) {
            query.setString(1, avio.getNom());
            query.setInt(2, avio.getnSeients());
            query.setString(3, avio.getModelAvio());
            query.setDate(4, java.sql.Date.valueOf(avio.getDataFabricacio()));
            query.setInt(5, avio.getCompanyia().getId());
            rowsUpdated = query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowsUpdated > 0;
    }

    /**
     * 
     * @param id
     * @return true si ha pogut eliminar, false si no
     */
    @Override
    public boolean delete(Integer id) {
        int rowsUpdated = 0;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(DELETE_STR)) {
            query.setInt(1, id);
            rowsUpdated = query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowsUpdated > 0;
    }

    /**
     * 
     * @return els objectes guardats en forma de map ordenat per l'id d'Avió
     */
    @Override
    public Map<Integer, Avio> getAll() {
        Map<Integer, Avio> map = null;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(SELECT_ALL_STR)) {
            map = mapQueryAvions(query);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     * 
     * @param companyia
     * @return els avions associats a la companyia.
     */
    @Override
    public Map<Integer, Avio> getAllByCompanyia(Companyia companyia) {
        Map<Integer, Avio> map = null;
        try (PreparedStatement query = JDBCConnection.getConnection().prepareStatement(SELECT_BY_AVIOID_STR)) {
            query.setInt(1, companyia.getId());
            map = mapQueryAvions(query);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    /**
     * 
     * @param query
     * @return les dades obtingudes de la consulta en forma de map 
     */
    private Map<Integer, Avio> mapQueryAvions(PreparedStatement query) {
        Map<Integer, Avio> map = null;

        try (ResultSet results = query.executeQuery()) {
            map = new TreeMap<>();
            while (results.next()) {
                map.put(results.getInt(1),
                        new Avio(results.getInt(1),
                                results.getInt("nSeients"),
                                results.getString("nom"),
                                results.getString("modelAvio"),
                                results.getDate("dataFabricacio").toLocalDate(),
                                null));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCAvioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }
}
