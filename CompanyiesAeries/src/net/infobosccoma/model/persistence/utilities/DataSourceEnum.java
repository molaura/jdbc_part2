/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.persistence.utilities;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public enum DataSourceEnum {
    JDBC, HIBERNATE, JSON
}
