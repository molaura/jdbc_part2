/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.business.entities;

import java.time.LocalDate;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public class Avio {
//<editor-fold defaultstate="collapsed" desc="Atributs d'instància">
    private int id, nSeients;
    private String nom, modelAvio;
    private LocalDate dataFabricacio;
    private Companyia companyia;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructors">
    public Avio(){
        this(0, 0, "", "", null);
    }
     public Avio(int nSeients, String nom, String modelAvio, LocalDate dataFabricacio){
        this(0, nSeients, nom, modelAvio, dataFabricacio);
    }

   public Avio(int id, int nSeients, String nom, String modelAvio, LocalDate dataFabricacio) {
        this(id, nSeients, nom, modelAvio, dataFabricacio, null);
    }
     
    public Avio(int id,int nSeients, String nom,  String modelAvio, LocalDate dataFabricacio, Companyia companyia) {
        this.id = 0;
        this.nom = nom;
        this.nSeients = nSeients;
        this.modelAvio = modelAvio;
        this.dataFabricacio = dataFabricacio;
        this.companyia = companyia;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Getters i Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getnSeients() {
        return nSeients;
    }

    public void setnSeients(int nSeients) {
        this.nSeients = nSeients;
    }

    public String getModelAvio() {
        return modelAvio;
    }

    public void setModelAvio(String modelAvio) {
        this.modelAvio = modelAvio;
    }

    public LocalDate getDataFabricacio() {
        return dataFabricacio;
    }

    public void setDataFabricacio(LocalDate dataFabricacio) {
        this.dataFabricacio = dataFabricacio;
    }

    public Companyia getCompanyia() {
        return companyia;
    }

    public void setCompanyia(Companyia companyia) {
        this.companyia = companyia;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Atributs d'instància">
//    @Override
//    public String toString() {
//        return String.format("ID: %d\nNUMERO DE SEIENTS: %d\nNOM: %s\nMODEL %s\n", this.id, this.nSeients, this.nom, this.modelAvio);
//        
//    }   

    @Override
    public String toString() {
        return "Avio{" + "id=" + id + ", nSeients=" + nSeients + ", nom=" + nom + ", modelAvio=" + modelAvio + ", dataFabricacio=" + dataFabricacio + ", companyia=" + companyia + '}';
    }
    //</editor-fold>
}
