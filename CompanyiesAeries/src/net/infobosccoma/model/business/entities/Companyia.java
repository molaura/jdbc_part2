/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.infobosccoma.model.business.entities;

import java.util.Map;

/**
 *
 * @author Laura Martinez && Sergi Formatgé
 */
public class Companyia {
    
//<editor-fold defaultstate="collapsed" desc="Atributs d'instància">
    private int id;
    private String nom;
    private Map<Integer, Avio> avions;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructors">
    public Companyia() {
        this("");
    }
    
    public Companyia(String nom) {
        this.nom = nom;
    }
    public Companyia(int id, String nom) {
        this(nom);
        this.id = id;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Getters i Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Map<Integer, Avio> getAvions(){
        return this.avions;
    }   

    public void setAvions(Map<Integer, Avio> avions) {
        this.avions = avions;
    }
    
    public void addAvio(Avio avio){
        this.avions.put(avio.getId(), avio);
    }
//</editor-fold>

    @Override
    public String toString() {
        return "Companyia{" + "ID = " + id + ", Nom = " + nom + '}';
    }    
}
