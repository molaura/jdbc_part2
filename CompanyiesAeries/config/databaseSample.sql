/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  usuari
 * Created: Feb 26, 2017
 */

CREATE TABLE "COMPANYIA" (
    "ID" NUMBER(*,0) NOT NULL ENABLE, 
    "NOM" VARCHAR2(100),  
    CONSTRAINT "PERSON_PK" PRIMARY KEY ("ID")
);

CREATE SEQUENCE  "COMPANYIASEQUENCIA" INCREMENT BY 1;


CREATE OR REPLACE TRIGGER COMPANYIATRIGGER 
BEFORE INSERT ON COMPANYIA 
   for each row 
BEGIN
  IF INSERTING THEN
  if :NEW."ID" is null then 
         select COMPANYIASEQUENCIA.nextval into :NEW."ID" from AVIONS; 
      end if; 
   end if;  
END;

/
ALTER TRIGGER "COMPANYIATRIGGER" ENABLE;

CREATE TABLE "AVIO" (
    "ID" NUMBER(*,0) NOT NULL ENABLE, 
    "NSEIENTS" NUMBER, 
    "NOM" VARCHAR2(100 BYTE), 
    "MODELAVIO" VARCHAR2(100 BYTE), 
    "DATAFABRICACIO" DATE,
    CONSTRAINT "AVIONS_PK" PRIMARY KEY ("ID")
    CONSTRAINT "AVIO_FK" FOREIGN KEY ("IDCOMPANYIA")
            REFERENCES "COMPANYIA" ("ID") ENABLE
);

CREATE SEQUENCE  "AVIO_SEQ" INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "AVIO_TRIGGER_NEW_ID" 
   before insert on "AVIO" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select AVIO_SEQ.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "AVIO_TRIGGER_NEW_ID" ENABLE;

